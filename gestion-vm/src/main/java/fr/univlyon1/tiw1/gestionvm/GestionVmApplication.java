package fr.univlyon1.tiw1.gestionvm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class GestionVmApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionVmApplication.class, args);
	}

}
