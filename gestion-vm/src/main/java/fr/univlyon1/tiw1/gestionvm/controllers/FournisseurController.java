package fr.univlyon1.tiw1.gestionvm.controllers;

import fr.univlyon1.tiw1.gestionvm.models.FournisseurDTO;
import fr.univlyon1.tiw1.gestionvm.services.NoSuchVMException;
import fr.univlyon1.tiw1.gestionvm.services.FournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(path = "/fournisseur")
public class FournisseurController {

    @Autowired
    private FournisseurService fournisseurService;

    @ResponseBody
    @PostMapping
    public ResponseEntity<FournisseurDTO> createFournisseur(@RequestBody FournisseurDTO fournisseurdto) {
          return new ResponseEntity(fournisseurService.createFournisseur(fournisseurdto), HttpStatus.CREATED);
    }

    @ResponseBody
    @GetMapping
    public Collection<FournisseurDTO> getAll() {
        return fournisseurService.getAllFournisseurs();
    }

    @GetMapping("/{nom}")
    public ResponseEntity<FournisseurDTO> getFournisseur(@PathVariable("nom") String nom) {
        try {
            return new ResponseEntity(fournisseurService.getFournisseur(nom), HttpStatus.OK);
        } catch (NoSuchVMException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{nom}")
    public ResponseEntity<FournisseurDTO> putFournisseur(@PathVariable("nom") String nom, @RequestBody FournisseurDTO fournisseurdto) {
        //if (fournisseurdto.getVms() != null)
           return new ResponseEntity(fournisseurService.putFournisseur(nom , fournisseurdto), HttpStatus.CREATED);
        /*else
            return new ResponseEntity(HttpStatus.NOT_FOUND);*/

    }


    @DeleteMapping("/{nom}")
    public ResponseEntity<FournisseurDTO> deleteFournisseur(@PathVariable("nom") String nom) {
        try {
            fournisseurService.deleteFournisseur(nom);
            return new ResponseEntity(null, HttpStatus.OK);
        } catch (NoSuchVMException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

}
