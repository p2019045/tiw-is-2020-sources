package fr.univlyon1.tiw1.gestionvm.controllers;

import fr.univlyon1.tiw1.gestionvm.models.VMDTO;
import fr.univlyon1.tiw1.gestionvm.services.NoSuchVMException;
import fr.univlyon1.tiw1.gestionvm.services.VMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(path = "/vm")
public class VMController {

    @Autowired
    private VMService vmService;

    @ResponseBody
    @PostMapping
    public ResponseEntity<VMDTO> createVM(@RequestBody VMDTO vmdto) {
        return new ResponseEntity(vmService.createVM(vmdto), HttpStatus.CREATED);
    }

    @ResponseBody
    @GetMapping
    public Collection<VMDTO> getAll() {
        return vmService.getAllVMs();
    }

    @GetMapping("/{id}")
    public ResponseEntity<VMDTO> getVM(@PathVariable("id") long id) {
        try {
            return new ResponseEntity(vmService.getVM(id), HttpStatus.OK);
        } catch (NoSuchVMException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<VMDTO> putVM(@PathVariable("id") long id, @RequestBody VMDTO vmdto) {
        return new ResponseEntity(vmService.putVM(id , vmdto), HttpStatus.CREATED);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<VMDTO> deleteVM(@PathVariable("id") long id) {
        try {
            vmService.deleteVM(id);
            return new ResponseEntity(null, HttpStatus.OK);
        } catch (NoSuchVMException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

}
