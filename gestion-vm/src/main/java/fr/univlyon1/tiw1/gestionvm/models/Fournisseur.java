package fr.univlyon1.tiw1.gestionvm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;
import java.util.Collection;
import javax.persistence.*;
import java.util.stream.Collectors;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;

@Entity
public class Fournisseur {
    @Id
    private String nom;

    private int quota_cpu;

    private int quota_ram;

    private int quota_disque;

    @OneToMany(mappedBy="fournisseur")
    Collection<VM> vms;




    /**
     * Internal nom used to identify the machine.
     * @return the nom
     */

    public String getNom() {
        return nom;
    }

    /**
     * Sets the internal nom used to identify this fournisseur.
     * Should not be set manually.
     * @param nom th nom to set.
     */

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Quota des cpu
     * @return quota des cpu
     */

    public int getQuota_cpu() {
        return quota_cpu;
    }

    /**
     *change cpu quota
     * @param nouvelle quota des cpu
     */

    public void setQuota_cpu(int quota_cpu) {
        this.quota_cpu = quota_cpu;
    }

    /**
     * Quota des ram
     * @return quota des ram
     */


    public int getQuota_ram() {
        return quota_ram;
    }

    /**
     *change ram quota
     * @param nouvelle quota des ram
     */
    public void setQuota_ram(int quota_ram) {
        this.quota_ram = quota_ram;
    }

    /**
     * Quota des ram
     * @return quota des disques
     */
    public int getQuota_disque() {
        return quota_disque;
    }

    /**
     *change disque quota
     * @param nouvelle quota des disques
     */
    public void setQuota_disque(int quota_disque) {
        this.quota_disque = quota_disque;
    }

    public Collection<VM> getVms() {
        return vms;
    }

    public void setVms(Collection<VM> vms) {
        this.vms = vms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fournisseur fournisseur = (Fournisseur) o;
        return Objects.equals(nom, fournisseur.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }

    public FournisseurDTO asDTO() {
        FournisseurDTO fournisseurDTO = new FournisseurDTO();
        fournisseurDTO.setNom(nom);
        fournisseurDTO.setQuota_cpu(quota_cpu);
        fournisseurDTO.setQuota_ram(quota_ram);
        fournisseurDTO.setQuota_disque(quota_disque);
        fournisseurDTO.setVms(vms.stream()
                .map(VM::getId)
                .collect(Collectors.toCollection(ArrayList::new)));
        return fournisseurDTO;
    }


}
