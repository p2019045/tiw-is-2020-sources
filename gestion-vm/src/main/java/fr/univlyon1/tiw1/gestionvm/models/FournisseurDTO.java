package fr.univlyon1.tiw1.gestionvm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;
import javax.persistence.*;
import java.util.Collection;


public class FournisseurDTO {
    private String nom;
    private int quota_cpu;
    private int quota_ram;
    private int quota_disque;
    Collection<Long> vms;


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getQuota_cpu() {
        return quota_cpu;
    }

    public void setQuota_cpu(int quota_cpu) {
        this.quota_cpu = quota_cpu;
    }

    public int getQuota_ram() {
        return quota_ram;
    }

    public void setQuota_ram(int quota_ram) {
        this.quota_ram = quota_ram;
    }

    public int getQuota_disque() {
        return quota_disque;
    }

    public void setQuota_disque(int quota_disque) {
        this.quota_disque = quota_disque;
    }

    public Collection<Long> getVms() {
        return vms;
    }

    public void setVms(Collection<Long> vms) {
        this.vms = vms;
    }

    public Fournisseur asFournisseur() {
        Fournisseur fournisseur = new Fournisseur();
        fournisseur.setNom(nom);
        fournisseur.setQuota_cpu(quota_cpu);
        fournisseur.setQuota_ram(quota_ram);
        fournisseur.setQuota_disque(quota_disque);
        return fournisseur;
    }
}
