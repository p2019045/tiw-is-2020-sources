package fr.univlyon1.tiw1.gestionvm.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * JPA repository to manage persisted VM entities.
 */
@Repository
public interface FournisseurRepository extends JpaRepository<Fournisseur,String> {
}
