package fr.univlyon1.tiw1.gestionvm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;
import javax.persistence.*;

/**
 * Represents a virtual machine to manage.
 */
@Entity
public class VM {
    @Id
    @GeneratedValue
    private Long id;

    private String ip;

    private String os;

    @ManyToOne
    @JoinColumn(nullable=false)
    private Fournisseur fournisseur;

    private int disque;
    private int ram;
    private int cpu;

    /**
     * Internal id used to identify the machine.
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the internal id used to identify this machine.
     * Should not be set manually.
     * @param id th id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * The machine's IP address.
     * @return the machine's IP address
     */
    public String getIp() {
        return ip;
    }

    /**
     * Changes the machine's IP address.
     * @param ip the new IP address.
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * The operating system of the machine.
     * @return the operating system name.
     */
    public String getOs() {
        return os;
    }

    /**
     * Changes the machine's operating system.
     * @param os The new operating system.
     */
    public void setOs(String os) {
        this.os = os;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public int getDisque() {
        return disque;
    }

    public void setDisque(int disque) {
        this.disque = disque;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getCpu() {
        return cpu;
    }

    public void setCpu(int cpu) {
        this.cpu = cpu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VM vm = (VM) o;
        return Objects.equals(id, vm.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public VMDTO asDTO() {
        VMDTO dto = new VMDTO();
        dto.setId(id);
        dto.setIp(ip);
        dto.setOs(os);
        dto.setCpu(cpu);
        dto.setRam(ram);
        dto.setDisque(disque);
        dto.setFournisseur(fournisseur.getNom());


        return dto;
    }
}
