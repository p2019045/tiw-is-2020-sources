package fr.univlyon1.tiw1.gestionvm.models;
import fr.univlyon1.tiw1.gestionvm.services.FournisseurService;
import fr.univlyon1.tiw1.gestionvm.services.NoSuchVMException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


/**
 * Serialization-ready representation of a virtual machine.
 */
public class VMDTO {
    private Long id;
    private String ip;
    private String os;
    private String fournisseur;
    private int disque;
    private int ram;
    private int cpu;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }

    public int getDisque() {
        return disque;
    }

    public void setDisque(int disque) {
        this.disque = disque;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getCpu() {
        return cpu;
    }

    public void setCpu(int cpu) {
        this.cpu = cpu;
    }

    public VM asVM() {
        VM vm = new VM();
        vm.setId(id);
        vm.setIp(ip);
        vm.setOs(os);
        vm.setCpu(cpu);
        vm.setRam(ram);
        vm.setDisque(disque);


        return vm;
    }
}
