package fr.univlyon1.tiw1.gestionvm.services;

import fr.univlyon1.tiw1.gestionvm.models.Fournisseur;
import fr.univlyon1.tiw1.gestionvm.models.FournisseurDTO;
import fr.univlyon1.tiw1.gestionvm.models.FournisseurRepository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Class holding VM management code.
 */
@Service
@Slf4j
public class FournisseurService {
    @Autowired
    private FournisseurRepository fournisseurRepository;

    /**
     * Creates a new VM and give it an id.
     *
     * @param vmdto the VM's information
     * @return vmdto updated with the VM's id.
     */
    @Transactional
    public FournisseurDTO createFournisseur(FournisseurDTO fournisseurDTO) {
        Fournisseur fournisseur = fournisseurDTO.asFournisseur();
       // try {
            fournisseur = fournisseurRepository.save(fournisseur);
      /*  } catch (EntityNotFoundException e) {
        throw new NoSuchVMException("L'ajout du fournisseur a echoue , verifie si le fournisseur existe deja", e);
        }*/


        return fournisseur.asDTO();
    }

    @Transactional
    public FournisseurDTO putFournisseur(String nom , FournisseurDTO fournisseurDTO) {
        Fournisseur fournisseur = fournisseurDTO.asFournisseur();
        fournisseur.setNom(nom);
        fournisseur = fournisseurRepository.save(fournisseur);
        return fournisseur.asDTO();
    }

    /**
     * Retreive all the VMs
     */
    public Collection<FournisseurDTO> getAllFournisseurs() {
        return fournisseurRepository
                .findAll()
                .stream()
                .map(Fournisseur::asDTO)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public FournisseurDTO getFournisseur(String nom) throws NoSuchVMException {
        try {
            Fournisseur fournisseur = fournisseurRepository.getOne(nom);
            if (fournisseur == null) {
                throw new NoSuchVMException("Fournisseur " + nom + " not found");
            }
            return fournisseur.asDTO();
        } catch (EntityNotFoundException e) {
            throw new NoSuchVMException("Fournisseur " + nom + " not found", e);
        }
    }

    public void deleteFournisseur(String nom) throws NoSuchVMException {
        try {
            /*Fournisseur fournisseur = fournisseurRepository.getOne(nom);
            if (fournisseur == null) {
                throw new NoSuchVMException("Fournisseur " + nom + " not found");
            }*/
            fournisseurRepository.deleteById(nom);
        } catch (EntityNotFoundException e) {
            throw new NoSuchVMException("Fournisseur " + nom + " not found", e);
        }
    }

}
