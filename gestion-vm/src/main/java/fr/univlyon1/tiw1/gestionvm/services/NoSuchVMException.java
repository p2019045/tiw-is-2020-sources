package fr.univlyon1.tiw1.gestionvm.services;

public class NoSuchVMException extends Exception {
    public NoSuchVMException() {
    }

    public NoSuchVMException(String message) {
        super(message);
    }

    public NoSuchVMException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchVMException(Throwable cause) {
        super(cause);
    }

    public NoSuchVMException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
