package fr.univlyon1.tiw1.gestionvm.services;

import fr.univlyon1.tiw1.gestionvm.models.VM;
import fr.univlyon1.tiw1.gestionvm.models.VMDTO;
import fr.univlyon1.tiw1.gestionvm.models.VMRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import fr.univlyon1.tiw1.gestionvm.models.FournisseurRepository;
import fr.univlyon1.tiw1.gestionvm.models.Fournisseur;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import fr.univlyon1.tiw1.gestionvm.models.Fournisseur;


/**
 * Class holding VM management code.
 */
@Service
@Slf4j
public class VMService {
    @Autowired
    private VMRepository vmRepository;
    @Autowired
    private FournisseurRepository fournisseurRepository;

    /**
     * Creates a new VM and give it an id.
     *
     * @param vmdto the VM's information
     * @return vmdto updated with the VM's id.
     */
    @Transactional
    public VMDTO createVM(VMDTO vmdto) {
        Fournisseur fournisseur = fournisseurRepository.getOne(vmdto.getFournisseur());
        System.out.println("fournisseur : "+fournisseur);
        if (fournisseur == null) {
            System.out.println("not found");
            return null;
        }
        VM vm = vmdto.asVM();
        vm.setId(null);
        vm.setFournisseur(fournisseurRepository.getOne(vmdto.getFournisseur()));
        vm = vmRepository.save(vm);
        return vm.asDTO();
    }


    public VMDTO putVM(long id , VMDTO vmdto) {
        VM vmupdate = vmRepository.getOne(id);
        if(vmupdate == null || vmdto.getFournisseur() != null)
            return null;

        VM vm = vmdto.asVM();
        vm.setId(id);
        vm.setFournisseur(fournisseurRepository.getOne(vmupdate.getFournisseur().getNom()));

        vm = vmRepository.save(vm);
        return vm.asDTO();
    }

    /**
     * Retreive all the VMs
     */
    public Collection<VMDTO> getAllVMs() {
        return vmRepository
                .findAll()
                .stream()
                .map(VM::asDTO)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public VMDTO getVM(long id) throws NoSuchVMException {
        try {
            VM vm = vmRepository.getOne(id);
            if (vm == null) {
                throw new NoSuchVMException("VM " + id + " not found");
            }
            return vm.asDTO();
        } catch (EntityNotFoundException e) {
            throw new NoSuchVMException("VM " + id + " not found", e);
        }
    }

    public void deleteVM(long id) throws NoSuchVMException {
        try {
            VM vm = vmRepository.getOne(id);
            if (vm == null) {
                throw new NoSuchVMException("VM " + id + " not found");
            }
            vmRepository.deleteById(id);
        } catch (EntityNotFoundException e) {
            throw new NoSuchVMException("VM " + id + " not found", e);
        }
    }

}
